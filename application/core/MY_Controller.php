<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $data;
    private $template;

	public function __construct()
    {
        parent::__construct();
        $this->data['page_title'] = "";
        $this->data['active_page'] = $this->uri->segment(1,'dashboard').DIRECTORY_SEPARATOR.$this->uri->segment(2,'index');
        $this->data['sub_menu'] = array();

        $this->template = "shared/layout";
    }

    public function addSection($section)
    {
        $this->data['section'][] = $this->load->view($section,$this->data,true);
    }

    public function loadStyle($stylesheet)
    {
        //the location of the script is to be placed at assets/css/
        $this->data['stylesheets'][] = $stylesheet;
    }

    public function loadScript($script)
    {
        //the location of the script is to be placed at assets/js/
        $this->data['scripts'][] = $script;
    }

    public function noTemplate()
    {
        $this->template = null;
    }

    public function alternateTemplate($template)
    {
        $this->template = $template;
    }

    public function _output()
    {
        if(!empty($this->template)){
            echo $this->load->view($this->template,$this->data,true);
        }else{
            if(isset($this->data['section'])) foreach($this->data['section'] as $idx => $content){
                echo $content;
            }            
        }
    }


}
