<?php

function debug($data,$dieAfter=true)
{
    echo "<pre>";
    print_r($data);
    if($dieAfter) die;
}