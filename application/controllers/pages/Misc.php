<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Misc extends MY_Controller {

	public function widgets()
	{
		$this->data['page_title'] = "Widgets";
		$this->data['active_page'] = "pages/misc/widgets";

		$this->addSection("pages/misc/widgets");
	}

	public function calendar()
	{
		$this->data['page_title'] = "Calendar";
		$this->data['active_page'] = "pages/misc/calendar";

		$this->loadStyle("AdminLTE-3/plugins/fullcalendar/main");
		$this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
		$this->loadScript("js/calendar");
		$this->addSection("pages/misc/calendar");
	}

	public function gallery()
	{
		$this->data['page_title'] = "Gallery";
		$this->data['active_page'] = "pages/misc/gallery";

		//<!-- Ekko Lightbox -->
		$this->loadStyle("AdminLTE-3/plugins/ekko-lightbox/ekko-lightbox");
		$this->loadScript("AdminLTE-3/plugins/ekko-lightbox/ekko-lightbox.min");
		$this->loadScript("AdminLTE-3/plugins/filterizr/jquery.filterizr.min");
		
		$this->loadScript("js/gallery");
		$this->addSection("pages/misc/gallery");
	}

	public function kanban()
	{
		$this->data['page_title'] = "Kanban";
		$this->data['active_page'] = "pages/misc/kanban";

		//<!-- Ekko Lightbox -->
		$this->loadStyle("AdminLTE-3/plugins/ekko-lightbox/ekko-lightbox");
		$this->loadScript("AdminLTE-3/plugins/ekko-lightbox/ekko-lightbox.min");
		$this->loadScript("AdminLTE-3/plugins/filterizr/jquery.filterizr.min");
		
		$this->addSection("pages/misc/kanban");
	}

}
