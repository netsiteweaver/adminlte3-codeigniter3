<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userinterface extends MY_Controller {

	public function index()
	{
		$this->data['page_title'] = "Home";
		
		// $this->loadStyle("AdminLTE-3/plugins/fullcalendar/main");
		// $this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
		// $this->loadScript("js/calendar");
		$this->addSection("home");
	}

	public function calendar()
	{
		$this->data['page_title'] = "Calendar";
		
		$this->loadStyle("AdminLTE-3/plugins/fullcalendar/main");
		$this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
		$this->loadScript("js/calendar");
		$this->addSection("calendar");
	}

	// public function index()
	// {
	// 	$this->data['page_title'] = "Calendar";
	// 	
	// 	$this->loadStyle("AdminLTE-3/plugins/fullcalendar/main");
	// 	$this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
	// 	$this->loadScript("js/calendar");
	// 	$this->addSection("calendar");
	// }

	// public function index()
	// {
	// 	$this->data['page_title'] = "Calendar";
	// 	
	// 	$this->loadStyle("AdminLTE-3/plugins/fullcalendar/main");
	// 	$this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
	// 	$this->loadScript("js/calendar");
	// 	$this->addSection("calendar");
	// }

	// public function index()
	// {
	// 	$this->data['page_title'] = "Calendar";
	// 	
	// 	$this->loadStyle("AdminLTE-3/plugins/fullcalendar/main");
	// 	$this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
	// 	$this->loadScript("js/calendar");
	// 	$this->addSection("calendar");
	// }

	
}
