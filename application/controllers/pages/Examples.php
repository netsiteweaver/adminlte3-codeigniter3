<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Examples extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->data['sub_menu'][] = "pages";
	}

	public function error404()
	{
		$this->data['page_title'] = "404 Page not found";
		$this->data['active_page'] = "pages/examples/error404";
		$this->data['body_class'] = "sidebar-mini";

		$this->data['sub_menu'] = ["extras"];

		$this->addSection("pages/examples/404");
	}

	public function error500()
	{
		$this->data['page_title'] = "500 Error";
		$this->data['active_page'] = "pages/examples/error500";
		$this->data['body_class'] = "sidebar-mini";

		$this->data['sub_menu'] = ["extras"];

		$this->addSection("pages/examples/500");
	}

	public function pace()
	{
		$this->data['page_title'] = "Pace";
		$this->data['active_page'] = "pages/examples/pace";
		$this->data['body_class'] = "sidebar-mini";

		$this->data['sub_menu'] = ["extras"];

		$this->addSection("pages/examples/pace");
	}

	public function blank()
	{
		$this->data['page_title'] = "Blank Page";
		$this->data['active_page'] = "pages/examples/blank";
		$this->data['body_class'] = "sidebar-mini";

		$this->data['sub_menu'] = ["extras"];

		$this->addSection("pages/examples/blank");
	}

	public function contact_us()
	{
		$this->data['page_title'] = "Contact us";
		$this->data['active_page'] = "pages/examples/contact-us";

		$this->addSection("pages/examples/contact-us");
	}

	public function contacts()
	{
		$this->data['page_title'] = "Contacts";
		$this->data['active_page'] = "pages/examples/contacts";

		$this->addSection("pages/examples/contacts");
	}

	public function e_commerce()
	{
		$this->data['page_title'] = "E-commerce";
		$this->data['active_page'] = "pages/examples/e-commerce";
		$this->data['body_class'] = "sidebar-mini";

		$this->addSection("pages/examples/e-commerce");
	}

	public function faq()
	{
		$this->data['page_title'] = "FAQ";
		$this->data['active_page'] = "pages/examples/faq";
		$this->data['body_class'] = "sidebar-mini";

		$this->addSection("pages/examples/faq");
	}

	public function forgot_password_v2()
	{
		$this->noTemplate();
		$this->addSection("pages/examples/forgot-password-v2");
	}

	public function forgot_password()
	{
		$this->noTemplate();
		$this->addSection("pages/examples/forgot-password");
	}

	public function invoice_print()
	{
		$this->data['page_title'] = "Invoice Print";
		$this->data['active_page'] = "pages/examples/invoice-print";
		$this->data['body_class'] = "sidebar-mini";

		// $this->loadStyle("AdminLTE-3/plugins/icheck-bootstrap/icheck-bootstrap.min");
		// $this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
		$this->loadScript("js/print");
		$this->addSection("pages/examples/invoice-print");
	}

	public function invoice()
	{
		$this->data['page_title'] = "Invoice";
		$this->data['active_page'] = "pages/examples/invoice";
		$this->data['body_class'] = "sidebar-mini";

		$this->addSection("pages/examples/invoice");
	}

	public function language_menu()
	{
		$this->noTemplate();
		$this->addSection("pages/examples/language-menu");
	}

	public function legacy_user_menu()
	{
		$this->noTemplate();
		$this->addSection("pages/examples/legacy-user-menu");
	}

	public function lockscreen()
	{
		$this->noTemplate();
		$this->addSection("pages/examples/lockscreen");
	}

	public function login_v2()
	{
		$this->noTemplate();
		$this->addSection("pages/examples/login-v2");
	}

	public function login()
	{
		$this->noTemplate();
		$this->addSection("pages/examples/login");
	}

	public function profile()
	{
		$this->data['page_title'] = "User Profile";
		$this->data['active_page'] = "pages/examples/profile";
		$this->data['body_class'] = "sidebar-mini";

		$this->addSection("pages/examples/profile");
	}

	public function project_add()
	{
		$this->data['page_title'] = "Project Add";
		$this->data['active_page'] = "pages/examples/project-add";
		$this->data['body_class'] = "sidebar-mini";

		$this->addSection("pages/examples/project-add");
	}

	public function project_detail()
	{
		$this->data['page_title'] = "Project Detail";
		$this->data['active_page'] = "pages/examples/project-detail";
		$this->data['body_class'] = "sidebar-mini";

		$this->addSection("pages/examples/project-detail");
	}

	public function project_edit()
	{
		$this->data['page_title'] = "Project Edit";
		$this->data['active_page'] = "pages/examples/project-edit";
		$this->data['body_class'] = "sidebar-mini";

		$this->addSection("pages/examples/project-edit");
	}

	public function projects()
	{
		$this->data['page_title'] = "Projects";
		$this->data['active_page'] = "pages/examples/projects";
		$this->data['body_class'] = "sidebar-mini";

		$this->addSection("pages/examples/projects");
	}

	public function recover_password_v2()
	{
		$this->noTemplate();
		$this->addSection("pages/examples/recover-password-v2");
	}

	public function recover_password()
	{
		$this->noTemplate();
		$this->addSection("pages/examples/recover-password");
	}

	public function register_v2()
	{
		$this->noTemplate();
		$this->addSection("pages/examples/register-v2");
	}

	public function register()
	{
		$this->noTemplate();
		$this->addSection("pages/examples/register");
	}
}
