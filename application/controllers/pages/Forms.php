<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forms extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->data['sub_menu'][] = "forms";
	}

	public function index()
	{
		$this->data['page_title'] = "Home";
		
		// $this->loadStyle("AdminLTE-3/plugins/fullcalendar/main");
		// $this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
		// $this->loadScript("js/calendar");
		$this->addSection("home");
	}

	public function calendar()
	{
		$this->data['page_title'] = "Calendar";
		
		$this->loadStyle("AdminLTE-3/plugins/fullcalendar/main");
		$this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
		$this->loadScript("js/calendar");
		$this->addSection("calendar");
	}

	// public function index()
	// {
	// 	$this->data['page_title'] = "Calendar";
	// 	
	// 	$this->loadStyle("AdminLTE-3/plugins/fullcalendar/main");
	// 	$this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
	// 	$this->loadScript("js/calendar");
	// 	$this->addSection("calendar");
	// }

	// public function index()
	// {
	// 	$this->data['page_title'] = "Calendar";
	// 	
	// 	$this->loadStyle("AdminLTE-3/plugins/fullcalendar/main");
	// 	$this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
	// 	$this->loadScript("js/calendar");
	// 	$this->addSection("calendar");
	// }

	// public function index()
	// {
	// 	$this->data['page_title'] = "Calendar";
	// 	
	// 	$this->loadStyle("AdminLTE-3/plugins/fullcalendar/main");
	// 	$this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
	// 	$this->loadScript("js/calendar");
	// 	$this->addSection("calendar");
	// }

	
}
