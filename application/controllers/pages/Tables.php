<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tables extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->data['sub_menu'][] = "tables";
	}

	public function data()
	{
		$this->data['page_title'] = "Tables - Data";
		$this->data['active_page'] = "pages/tables/data";
		// $this->data['body_class'] = "sidebar-mini layout-navbar-fixed";

		//<!-- DataTables -->
		$this->loadStyle("AdminLTE-3/plugins/datatables-bs4/css/dataTables.bootstrap4.min");
		$this->loadStyle("AdminLTE-3/plugins/datatables-responsive/css/responsive.bootstrap4.min");
		$this->loadStyle("AdminLTE-3/plugins/datatables-buttons/css/buttons.bootstrap4.min");

		//<!-- DataTables  & Plugins -->
		$this->loadScript("AdminLTE-3/plugins/datatables/jquery.dataTables.min");
		$this->loadScript("AdminLTE-3/plugins/datatables-bs4/js/dataTables.bootstrap4.min");
		$this->loadScript("AdminLTE-3/plugins/datatables-responsive/js/dataTables.responsive.min");
		$this->loadScript("AdminLTE-3/plugins/datatables-responsive/js/responsive.bootstrap4.min");
		$this->loadScript("AdminLTE-3/plugins/datatables-buttons/js/dataTables.buttons.min");
		$this->loadScript("AdminLTE-3/plugins/datatables-buttons/js/buttons.bootstrap4.min");
		$this->loadScript("AdminLTE-3/plugins/jszip/jszip.min");
		$this->loadScript("AdminLTE-3/plugins/pdfmake/pdfmake.min");
		$this->loadScript("AdminLTE-3/plugins/pdfmake/vfs_fonts");
		$this->loadScript("AdminLTE-3/plugins/datatables-buttons/js/buttons.html5.min");
		$this->loadScript("AdminLTE-3/plugins/datatables-buttons/js/buttons.print.min");
		$this->loadScript("AdminLTE-3/plugins/datatables-buttons/js/buttons.colVis.min");
		$this->loadScript("js/tables-data");
		$this->addSection("pages/tables/data");
	}

	public function jsgrid()
	{
		$this->data['page_title'] = "Tables - jsGrid";
		$this->data['active_page'] = "pages/tables/jsgrid";
		// $this->data['body_class'] = "sidebar-mini layout-navbar-fixed";
		
		//<!-- jsGrid -->
		$this->loadStyle("AdminLTE-3/plugins/jsgrid/jsgrid.min");
		$this->loadStyle("AdminLTE-3/plugins/jsgrid/jsgrid-theme.min");

		//<!-- jsGrid -->
		$this->loadScript("AdminLTE-3/plugins/jsgrid/demos/db");
		$this->loadScript("AdminLTE-3/plugins/jsgrid/jsgrid.min");
		$this->loadScript("AdminLTE-3/dist/js/demo");
		$this->loadScript("js/tables-jsgrid");
		$this->addSection("pages/tables/jsgrid");
	}

	public function simple()
	{
		$this->data['page_title'] = "Tables - jsGrid";
		$this->data['active_page'] = "pages/tables/simple";
		// $this->data['body_class'] = "sidebar-mini layout-navbar-fixed";
		
		//<!-- jsGrid -->
		$this->loadStyle("AdminLTE-3/plugins/jsgrid/jsgrid.min");
		$this->loadStyle("AdminLTE-3/plugins/jsgrid/jsgrid-theme.min");
		
		//<!-- jsGrid -->
		$this->loadScript("AdminLTE-3/plugins/jsgrid/demos/db");
		$this->loadScript("AdminLTE-3/plugins/jsgrid/jsgrid.min");
		$this->loadScript("AdminLTE-3/dist/js/demo");
		$this->loadScript("js/tables-jsgrid");
		$this->addSection("pages/tables/simple");
	}
	
}
