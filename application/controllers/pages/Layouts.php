<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layouts extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->data['sub_menu'][] = "layout_options";
	}

	public function top_nav()
	{
		$this->data['page_title'] = "Top Bar";
		$this->data['active_page'] = "pages/layouts/top-nav";
		$this->data['body_class'] = "layout-top-nav";
		
		$this->addSection("pages/layouts/top-nav");
	}

	public function top_nav_sidebar()
	{
		$this->data['page_title'] = "Top Nav Sidebar";
		$this->data['active_page'] = "pages/layouts/top-nav-sidebar";
		$this->data['body_class'] = "sidebar-collapse layout-top-nav";
		
		$this->addSection("pages/layouts/top-nav-sidebar");
	}

	public function boxed()
	{
		$this->data['page_title'] = "Boxed Layout";
		$this->data['active_page'] = "pages/layouts/boxed";
		$this->data['body_class'] = "sidebar-mini layout-boxed";
		
		$this->addSection("pages/layouts/boxed");
	}	

	public function fixed_topnav()
	{
		$this->data['page_title'] = "Fixed Navbar Layout";
		$this->data['active_page'] = "pages/layouts/fixed-topnav";
		$this->data['body_class'] = "sidebar-mini layout-navbar-fixed";
		
		$this->addSection("pages/layouts/fixed-topnav");
	}	

	public function fixed_sidebar()
	{
		$this->data['page_title'] = "Fixed Sidebar";
		$this->data['active_page'] = "pages/layouts/fixed-sidebar";
		$this->data['body_class'] = "sidebar-mini layout-fixed";

		$this->loadStyle("AdminLTE-3/plugins/overlayScrollbars/css/OverlayScrollbars.min");
		$this->loadScript("AdminLTE-3/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min");
		
		$this->addSection("pages/layouts/fixed-sidebar");
	}	

	public function fixed_sidebar_custom()
	{
		$this->data['page_title'] = "Fixed Sidebar";
		$this->data['active_page'] = "pages/layouts/fixed-sidebar-custom";
		$this->data['body_class'] = "sidebar-mini layout-fixed";
		
		$this->loadStyle("AdminLTE-3/plugins/overlayScrollbars/css/OverlayScrollbars.min");
		$this->addSection("pages/layouts/fixed-sidebar-custom");
	}	

	public function fixed_footer()
	{
		$this->data['page_title'] = "Fixed Footer Layout";
		$this->data['active_page'] = "pages/layouts/fixed-footer";
		$this->data['body_class'] = "sidebar-mini layout-footer-fixed";
		
		$this->addSection("pages/layouts/fixed-footer");
	}	

	public function collapsed_sidebar()
	{
		$this->data['page_title'] = "Collapsed Sidebar";
		$this->data['active_page'] = "pages/layouts/collapsed-sidebar";
		$this->data['body_class'] = "sidebar-mini sidebar-collapse";
		
		$this->addSection("pages/layouts/collapsed-sidebar");
	}	
}
