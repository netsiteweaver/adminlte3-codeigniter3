<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailbox extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->data['sub_menu'][] = "mailbox";
	}

	public function list_mail()
	{
		$this->data['page_title'] = "Mailbox";
		$this->data['active_page'] = "pages/mailbox/list_mail";
		$this->data['body_class'] = "sidebar-mini";

		$this->loadStyle("AdminLTE-3/plugins/icheck-bootstrap/icheck-bootstrap.min");
		$this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
		$this->loadScript("js/mailbox-mailbox");
		$this->addSection("pages/mailbox/mailbox");
	}

	public function compose()
	{
		$this->data['page_title'] = "Compose Message";
		$this->data['active_page'] = "pages/mailbox/compose";
		$this->data['body_class'] = "sidebar-mini";

		$this->loadStyle("AdminLTE-3/plugins/summernote/summernote-bs4.min.css");
		$this->loadScript("AdminLTE-3/plugins/summernote/summernote-bs4.min.js");
		$this->loadScript("js/mailbox-compose");
		$this->addSection("pages/mailbox/compose");
	}

	public function read_mail()
	{
		$this->data['page_title'] = "Read Mail";
		$this->data['active_page'] = "pages/mailbox/read-mail";
		$this->data['body_class'] = "sidebar-mini";

		$this->loadStyle("AdminLTE-3/plugins/fullcalendar/main");
		$this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
		// $this->loadScript("js/calendar");
		$this->addSection("pages/mailbox/read-mail");
	}
}
