<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Charts extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->data['sub_menu'][] = "charts";
	}

	public function chartjs()
	{
		$this->data['page_title'] = "ChartJS";
		$this->data['active_page'] = "pages/charts/chartjs";
		$this->data['body_class'] = "sidebar-mini";

		$this->loadScript("AdminLTE-3/plugins/chart.js/Chart.min");
		$this->loadScript("js/charts-chartjs");
		$this->addSection("pages/charts/chartjs");
	}

	public function flot()
	{
		$this->data['page_title'] = "Flot Charts";
		$this->data['active_page'] = "pages/charts/flot";
		$this->data['body_class'] = "sidebar-mini";

		//<!-- FLOT CHARTS -->
		$this->loadScript("AdminLTE-3/plugins/flot/jquery.flot");
		//<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
		$this->loadScript("AdminLTE-3/plugins/flot/plugins/jquery.flot.resize");
		//<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
		$this->loadScript("AdminLTE-3/plugins/flot/plugins/jquery.flot.pie");

		$this->loadScript("js/charts-flot");
		$this->addSection("pages/charts/flot");
	}

	public function inline()
	{
		$this->data['page_title'] = "Inline Charts";
		$this->data['active_page'] = "pages/charts/inline";
		$this->data['body_class'] = "sidebar-mini";

		$this->loadStyle("AdminLTE-3/plugins/icheck-bootstrap/icheck-bootstrap.min");
		// <!-- jQuery Knob -->
		$this->loadScript("AdminLTE-3/plugins/jquery-knob/jquery.knob.min");
		// <!-- Sparkline -->
		$this->loadScript("AdminLTE-3/plugins/sparklines/sparkline");
		$this->loadScript("js/charts-inline");
		$this->addSection("pages/charts/inline");
	}

	public function uplot()
	{
		$this->data['page_title'] = "uPlot";
		$this->data['active_page'] = "pages/charts/uplot";
		$this->data['body_class'] = "sidebar-mini";

		$this->loadStyle("AdminLTE-3/plugins/uplot/uPlot.min");
		$this->loadScript("AdminLTE-3/plugins/uplot/uPlot.iife.min");
		$this->loadScript("js/charts-uplot");
		$this->addSection("pages/charts/uplot");
	}
	
}
