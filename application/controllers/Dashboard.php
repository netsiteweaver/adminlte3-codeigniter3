<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->data['sub_menu'][] = "dashboard";

	}

	public function index()
	{
		$this->data['page_title'] = "Dashboard";

		// <!-- Tempusdominus Bootstrap 4 -->
		$this->loadStyle("AdminLTE-3/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min");
		// <!-- iCheck -->
		$this->loadStyle("AdminLTE-3/plugins/icheck-bootstrap/icheck-bootstrap.min");
		// <!-- JQVMap -->
		$this->loadStyle("AdminLTE-3/plugins/jqvmap/jqvmap.min");
		// <!-- Theme style -->
		$this->loadStyle("AdminLTE-3/dist/css/adminlte.min");
		// <!-- overlayScrollbars -->
		$this->loadStyle("AdminLTE-3/plugins/overlayScrollbars/css/OverlayScrollbars.min");
		// <!-- Daterange picker -->
		$this->loadStyle("AdminLTE-3/plugins/daterangepicker/daterangepicker");
		// <!-- summernote -->
		$this->loadStyle("AdminLTE-3/plugins/summernote/summernote-bs4.min");

		$this->loadScript("AdminLTE/");
		// <!-- ChartJS -->
		$this->loadScript("AdminLTE/plugins/chart.js/Chart.min");
		// <!-- Sparkline -->
		$this->loadScript("AdminLTE/plugins/sparklines/sparkline");// <script src=".js"></script>
		// <!-- JQVMap -->
		$this->loadScript("AdminLTE/plugins/jqvmap/jquery.vmap.min");// <script src=".js"></script>
		$this->loadScript("AdminLTE/plugins/jqvmap/maps/jquery.vmap.usa");// <script src=".js"></script>
		// <!-- jQuery Knob Chart -->
		$this->loadScript("AdminLTE/plugins/jquery-knob/jquery.knob.min");// <script src=".js"></script>
		// <!-- daterangepicker -->
		$this->loadScript("AdminLTE/plugins/moment/moment.min");// <script src=".js"></script>
		$this->loadScript("AdminLTE/plugins/daterangepicker/daterangepicker");// <script src=".js"></script>
		// <!-- Tempusdominus Bootstrap 4 -->
		$this->loadScript("AdminLTE/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min");// <script src=".js"></script>
		// <!-- Summernote -->
		$this->loadScript("AdminLTE/plugins/summernote/summernote-bs4.min");// <script src=".js"></script>
		// <!-- overlayScrollbars -->
		$this->loadScript("AdminLTE/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min");// <script src=".js"></script>
		
		$this->addSection("dashboard/index.php");
	}

	public function index2()
	{
		$this->data['page_title'] = "Dashboard 2";
		
		$this->loadStyle("AdminLTE-3/plugins/fullcalendar/main");
		$this->loadScript("AdminLTE-3/plugins/fullcalendar/main");
		$this->loadScript("js/calendar");

		//<!-- jQuery Mapael -->
		$this->loadScript("AdminLTE-3/plugins/jquery-mousewheel/jquery.mousewheel");
		$this->loadScript("AdminLTE-3/plugins/raphael/raphael.min");
		$this->loadScript("AdminLTE-3/plugins/jquery-mapael/jquery.mapael.min");
		$this->loadScript("AdminLTE-3/plugins/jquery-mapael/maps/usa_states.min");
		//<!-- ChartJS -->
		$this->loadScript("AdminLTE-3/plugins/chart.js/Chart.min");
		$this->loadScript("AdminLTE-3/dist/js/demo.js");
		$this->loadScript("AdminLTE-3/dist/js/pages/dashboard2");
		$this->loadScript("AdminLTE-3/");

		$this->addSection("dashboard/index2.php");
	}

	public function index3()
	{
		$this->data['page_title'] = "Dashboard 3x";
		
		$this->loadScript("AdminLTE-3/plugins/chart.js/Chart.min");
		$this->loadScript("AdminLTE-3/dist/js/pages/dashboard3");
		$this->addSection("dashboard/index3.php");
	}
	
}
