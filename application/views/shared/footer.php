<footer class="main-footer">
    <strong>Copyright &copy; 2014-2020 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.1.0-rc
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="assets/AdminLTE-3/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="assets/AdminLTE-3/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="assets/AdminLTE-3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<!-- <script src="assets/AdminLTE-3/plugins/chart.js/Chart.min.js"></script> -->
<!-- Sparkline -->
<!-- <script src="assets/AdminLTE-3/plugins/sparklines/sparkline.js"></script> -->
<!-- JQVMap -->
<!-- <script src="assets/AdminLTE-3/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="assets/AdminLTE-3/plugins/jqvmap/maps/jquery.vmap.usa.js"></script> -->
<!-- jQuery Knob Chart -->
<!-- <script src="assets/AdminLTE-3/plugins/jquery-knob/jquery.knob.min.js"></script> -->
<!-- daterangepicker -->
<!-- <script src="assets/AdminLTE-3/plugins/moment/moment.min.js"></script> -->
<!-- <script src="assets/AdminLTE-3/plugins/daterangepicker/daterangepicker.js"></script> -->
<!-- Tempusdominus Bootstrap 4 -->
<!-- <script src="assets/AdminLTE-3/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script> -->
<!-- Summernote -->
<!-- <script src="assets/AdminLTE-3/plugins/summernote/summernote-bs4.min.js"></script> -->
<!-- overlayScrollbars -->
<script src="assets/AdminLTE-3/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="assets/AdminLTE-3/dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="assets/AdminLTE-3/dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="assets/AdminLTE-3/dist/js/pages/dashboard.js"></script> -->
<!-- dynamic scripts: loaded from controller -->
<?php if(!empty($scripts)) foreach($scripts as $script):?>
<?php if(file_exists(realpath('.')."/assets/$script.js")):?>
<script src="assets/<?php echo $script?>.js"></script>
<?php endif;?>
<?php endforeach;?>
</body>
</html>
