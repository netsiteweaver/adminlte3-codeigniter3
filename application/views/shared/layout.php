<?php $this->load->view("shared/header",$this->data); ?>
<?php $this->load->view("shared/nav",$this->data); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<?php if(isset($this->data['section'])) foreach($this->data['section'] as $idx => $content): ?>

  <?php echo $content; ?>
  
<?php endforeach;?>
</div>
  <!-- /.content-wrapper -->

<?php $this->load->view("shared/footer",$this->data); ?>
