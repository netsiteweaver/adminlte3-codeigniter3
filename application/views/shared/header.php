<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo (isset($page_title))?$page_title." | ":"";?>mailPUSH</title>
  <base href="<?php echo base_url();?>">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="assets/AdminLTE-3/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <!-- <link rel="stylesheet" href="assets/AdminLTE-3/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"> -->
  <!-- iCheck -->
  <!-- <link rel="stylesheet" href="assets/AdminLTE-3/plugins/icheck-bootstrap/icheck-bootstrap.min.css"> -->
  <!-- JQVMap -->
  <!-- <link rel="stylesheet" href="assets/AdminLTE-3/plugins/jqvmap/jqvmap.min.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/AdminLTE-3/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="assets/AdminLTE-3/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <!-- <link rel="stylesheet" href="assets/AdminLTE-3/plugins/daterangepicker/daterangepicker.css"> -->
  <!-- summernote -->
  <!-- <link rel="stylesheet" href="assets/AdminLTE-3/plugins/summernote/summernote-bs4.min.css"> -->
  <!-- dynamic stylesheets: loaded from controller -->
<?php if(!empty($stylesheets)) foreach($stylesheets as $stylesheet):?>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/<?php echo $stylesheet?>.css">
<?php endforeach;?>
  <!-- custom styles -->
  <link rel="stylesheet" href="assets/css/styles.css">
</head>
<body class="hold-transition <?php echo( (isset($body_class) && !empty($body_class)) )?$body_class:"sidebar-mini layout-fixed";?>">
<div class="wrapper">
